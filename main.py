import vehiculos
import aves


class Ciudad:
    def __init__(self, nombre):
        self.nombre = nombre
        self.vehiculos = []
        self.lavaderos = []

    def agregar_vehiculo(self, vehiculo: vehiculos.Vehiculo):
        self.vehiculos.append(vehiculo)

    def agregar_lavadero(self, lavadero: vehiculos.Lavadero):
        self.lavaderos.append(lavadero)

    def llover_ceniza(self, milimetros):
        print(f"Llovieron {milimetros} milimetros de ceniza")
        for vehiculo in self.vehiculos:
            vehiculo.ensuciarse(milimetros)

    def lavadero_mas_barato(self, vehiculo: vehiculos.Vehiculo):
        return min(self.lavaderos, key=lambda lavadero: lavadero.get_precio(vehiculo))


if __name__ == '__main__':
    # 1)
    bandada = aves.Bandada([aves.Gaviota(2), aves.Gaviota(3), aves.Paloma(20), aves.Ave(), aves.Ave(), aves.Ave()],
                           aves.FormacionV())

    # 2)
    buenos_aires = Ciudad("Buenos Aires")
    auto = vehiculos.Vehiculo()
    buenos_aires.agregar_vehiculo(auto)

    # 3)
    smallLav = vehiculos.Artesanal(3, 100)
    buenos_aires.agregar_lavadero(smallLav)

    # 4)
    paloma_gorda = aves.Paloma(50)
    paloma_gorda.ensuciar(auto)
    print(auto.suciedad)
    bandada.ensuciar(auto)
    print(auto.suciedad)
    buenos_aires.llover_ceniza(23)
    smallLav.lavar(auto)
    bandada.cambiar_formacion(aves.FormacionW())
    bandada.ensuciar(auto)
    buenos_aires.lavadero_mas_barato(auto).lavar(auto)
