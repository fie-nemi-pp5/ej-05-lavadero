class Vehiculo:
    def __init__(self, suciedad=0):
        self.suciedad = suciedad

    def ensuciarse(self, suciedad):
        self.suciedad += suciedad

    def lavarse(self):
        suciedad = self.suciedad
        self.suciedad = 0
        return suciedad


class Lavadero:
    def get_precio(self, vehiculo: Vehiculo):
        pass

    def tiempo(self, vehiculo: Vehiculo):
        pass

    def lavar(self, vehiculo: Vehiculo):
        precio = self.get_precio(vehiculo)
        print(f"Lavar el vehículo demoró {self.tiempo(vehiculo)} minutos y costó ${precio}")
        vehiculo.lavarse()
        return precio


class Automatico(Lavadero):
    def __init__(self, precio, tiempo):
        self.precio = precio
        self.tiempo = tiempo

    def get_precio(self, vehiculo: Vehiculo):
        return self.precio

    def tiempo(self, vehiculo: Vehiculo):
        return self.tiempo()


class Artesanal(Lavadero):
    def __init__(self, cant_personas, costo_unitario):
        self.cant_personas = cant_personas
        self.costo_unitario = costo_unitario

    def tiempo(self, vehiculo: Vehiculo):
        return vehiculo.suciedad / 5

    def get_precio(self, vehiculo: Vehiculo):
        return self.cant_personas * self.costo_unitario * self.tiempo(vehiculo)
