from vehiculos import *


class Ave:
    def ensuciar(self, vehiculo):
        vehiculo.ensuciarse(1)


class Gaviota(Ave):
    def __init__(self, cant_alimento):
        self.cant_alimento = cant_alimento

    def ensuciar(self, vehiculo):
        vehiculo.ensuciarse(self.cant_alimento * 3)


class Paloma(Ave):
    def __init__(self, peso):
        self.peso = peso

    def ensuciar(self, vehiculo):
        vehiculo.ensuciarse(self.peso * 0.3)
        self.peso -= self.peso * 0.3


class Formacion:
    def ensuciar(self, vehiculo, aves):
        pass


class FormacionV(Formacion):
    def ensuciar(self, vehiculo, aves):
        for ave in aves:
            ave.ensuciar(vehiculo)


class FormacionW(Formacion):
    def ensuciar(self, vehiculo, aves):
        for ave in aves:
            ave.ensuciar(vehiculo)
            ave.ensuciar(vehiculo)


class FormacionI(Formacion):
    def ensuciar(self, vehiculo, aves):
        aves[0].ensuciar(vehiculo)
        if len(aves) > 1:
            aves[-1].ensuciar(vehiculo)


class Bandada:
    def __init__(self, aves, formacion: Formacion):
        self.aves = aves
        self.formacion = formacion

    def cambiar_formacion(self, formacion: Formacion):
        self.formacion = formacion

    def ensuciar(self, vehiculo):
        self.formacion.ensuciar(vehiculo, self.aves)
